defmodule Bootstrap4ElixirWeb.PageController do
  use Bootstrap4ElixirWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
